from django.shortcuts import get_object_or_404, render,  redirect
from .models import Recipe
from .forms import RecipeForm
from django.contrib.auth.decorators import login_required

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

def edit_recipe(request, id):
    post = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=post)
        if form.is_valid():
            recipe = form.save()
            return redirect("list") #go back to main page with list of recipes // TO ADD INGREDIENTS AND ELEMENTS: REDIRECT TO ANOTHER PAGE THAT ADDS INGREDIENTS AND STEPS FORMS
    else:
        form = RecipeForm(instance=post)
    context = {
        "form_object": post,
        "form": form
    }
    return render(request, "recipes/edit.html", context)

@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            form.save()
            return redirect("list") #go back to main page with list of recipes
    else:
        form = RecipeForm()
    context = {
        "form": form
    }
    return render(request, "recipes/create.html", context)

def show_recipe(request, id):
    a_really_cool_recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": a_really_cool_recipe
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)
