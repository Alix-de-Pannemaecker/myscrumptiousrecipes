from django.urls import path
from .views import show_recipe, recipe_list, create_recipe, edit_recipe, my_recipe_list

urlpatterns = [
    path("", recipe_list, name = "list"),
    path("<int:id>/", show_recipe, name = "detail"),
    path("create/", create_recipe, name = "create"),
    path("<int:id>/edit/", edit_recipe, name = "edit"),
    path("mine/", my_recipe_list, name = "my_list"),
    ]
